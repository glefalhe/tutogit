# Géraud Le Falher

__PhD researcher__

_MAGNET Team_

[website](http://geraud.so)

## Dev

- Python
	- scikit learn
	- graph-tool
- Latex
- Git

My favorite linux command is `acpi -i`

## My top 3 TV Shows

1. Big Bang Theory
2. The Expanse
3. Silicon Valley

## My top 3 emoticons

 :smiling_imp: -  :panda_face: -  :cherry_blossom:

```
 :smiling_imp: -  :panda_face: -  :cherry_blossom:
```
